![示例图片](info.png)
# 抖音直播分发器

这是一个用于使用 FFmpeg 将抖音（TikTok）的直播流分发到多个目标 URL 的 Python 脚本。它允许您输入抖音直播间(WEB)的 URL 并将其分发到不同的平台或位置。

## 先决条件

在使用此脚本之前，请确保已安装以下依赖项：

- Python 3.11.5
- FFmpeg（请确保它在系统 PATH 中）

## 使用方法

按照以下步骤使用抖音直播流分发器：

1. 克隆或下载此存储库到您的本地计算机。

2. 如果尚未安装所需的 Python 包，请执行以下命令：

   ``bash
   pip install -r requirements.txt
   ``

3. 创建一个包含以下内容的 `config.ini` 文件：

   ```ini
   [rtmp]
   target_urls = ['rtmp://您的目标URL1', 'rtmp://您的目标URL2']
   ```

   请将 `'rtmp://您的目标URL1'` 和 `'rtmp://您的目标URL2'` 替换为您想要分发的目标 URL。

4. 使用以下命令运行脚本：

   ```bash
   python main.py -u <抖音直播间的URL>
   ```

   将 `<抖音直播间的URL>` 替换为您要分发的抖音直播流的 URL。

5. 脚本将使用 FFmpeg 开始将直播流分发到指定的目标 URL。

6. 要停止分发特定的流，请关闭相应的 FFmpeg 窗口。

## 功能特点

- 同时将抖音直播流分发到多个目标 URL。
- 通过 `config.ini` 文件轻松配置。
- 简单的命令行界面。

## 许可证

本项目根据 MIT 许可证授权 - 有关详细信息，请参阅 [LICENSE](LICENSE) 文件。

## 鸣谢

感谢 Gundy 提供的 Python 获取抖音直播间视频流的代码。

作者的文章地址：[https://zhuanlan.zhihu.com/p/600793041](https://zhuanlan.zhihu.com/p/600793041)

## 免责声明

此脚本仅供教育和测试目的。请遵抖音直播平台的服务条款。

**注意**：确保将 `<抖音直播间的URL>`、`'rtmp://您的目标URL1'` 和 `'rtmp://您的目标URL2'` 替换为实际的 URL 和文件路径。